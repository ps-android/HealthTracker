package com.example.disneycahracters.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Entity(tableName = "DisneyCharacters")
@Parcelize
data class DisneyCharacters (


  @SerializedName("data"       ) var data       : ArrayList<Data> = arrayListOf(),
  @PrimaryKey
  @SerializedName("count"      ) var count      : Int?            = null,
  @SerializedName("totalPages" ) var totalPages : Int?            = null,
  @SerializedName("nextPage"   ) var nextPage   : String?         = null

) : Parcelable