package com.example.disneycahracters.model


sealed class ResponseStates

object Loading : ResponseStates()

object ServiceError : ResponseStates()

class Success(var response : DisneyCharacters?) : ResponseStates()
