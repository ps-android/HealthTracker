package com.example.disneycahracters.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Entity(tableName = "DATA")
@Parcelize
data class Data (

  @SerializedName("films"           ) var films           : ArrayList<String> = arrayListOf(),
  @SerializedName("shortFilms"      ) var shortFilms      : ArrayList<String> = arrayListOf(),
  @SerializedName("tvShows"         ) var tvShows         : ArrayList<String> = arrayListOf(),
  @SerializedName("videoGames"      ) var videoGames      : ArrayList<String> = arrayListOf(),
  @SerializedName("parkAttractions" ) var parkAttractions : ArrayList<String> = arrayListOf(),
  @SerializedName("allies"          ) var allies          : ArrayList<String> = arrayListOf(),
  @SerializedName("enemies"         ) var enemies         : ArrayList<String> = arrayListOf(),
  @PrimaryKey
  @SerializedName("_id"             ) var Id              : Int?              = null,
  @SerializedName("name"            ) var name            : String?           = null,
  @SerializedName("imageUrl"        ) var imageUrl        : String?           = null,
  @SerializedName("url"             ) var url             : String?           = null

) : Parcelable