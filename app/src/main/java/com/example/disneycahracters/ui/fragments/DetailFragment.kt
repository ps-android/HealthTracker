package com.example.disneycahracters.ui.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.disneycahracters.model.Data
import com.example.disneycahracters.R
import com.example.disneycahracters.databinding.FragmentDetailBinding

class DetailFragment : Fragment() {

    private lateinit var binding: FragmentDetailBinding
    private lateinit var data: Data

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentDetailBinding.inflate(layoutInflater, container, false)
        val bundle = arguments
        data = bundle?.get("datakey") as Data
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpObservers()
    }

    private fun setUpObservers() {
        binding.productNameTV.text = data.name
        val requestOptions = RequestOptions()
        requestOptions.placeholder(R.color.shimmer_placeholder)
        requestOptions.error(R.color.shimmer_placeholder)

        Glide.with(binding.root)
            .setDefaultRequestOptions(requestOptions).load(data.imageUrl)
            .into(binding.productIV)
        binding.productIV.contentDescription = data.name + " image"
    }


}