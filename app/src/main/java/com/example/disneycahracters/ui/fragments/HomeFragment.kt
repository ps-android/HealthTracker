package com.example.disneycahracters.ui.fragments


import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import com.example.disneycahracters.model.Data
import com.example.disneycahracters.R
import com.example.disneycahracters.adapter.DisneyCharactersAdapter
import com.example.disneycahracters.databinding.FragmentHomeBinding
import com.example.disneycahracters.interfaces.AppFlowListener
import com.example.disneycahracters.model.Loading
import com.example.disneycahracters.model.ServiceError
import com.example.disneycahracters.model.Success
import com.example.disneycahracters.utils.Constants
import com.example.disneycahracters.viewmodel.DisneyCharacterViewModel
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class HomeFragment : Fragment() {
    private lateinit var binding: FragmentHomeBinding
    private val disneyCharacterViewModel: DisneyCharacterViewModel by viewModels()

    private var listener: AppFlowListener? = null

    @Inject
    lateinit var disneyCharactersAdapter: DisneyCharactersAdapter

    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener = context as? AppFlowListener
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentHomeBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.lottieship.contentDescription = Constants.LOTTIEDESCRIPTIONSTRING
        setUpObservers()
        handleClickEvents(view)
    }

    override fun onResume() {
        super.onResume()
        listener?.onHomeFragment()
    }

    private fun handleClickEvents(view: View) {
        disneyCharactersAdapter.setOnInteractionListener(object :
            DisneyCharactersAdapter.OnInteractionListener {
            override fun onItemClicked(data: Data) {
                val bundle = Bundle()
                bundle.putParcelable("datakey", data)
                Navigation.findNavController(view)
                    .navigate(R.id.action_homeFragment_to_detailFragment, bundle)
            }
        })
    }

    private fun setUpObservers() {
        disneyCharacterViewModel.getCartScreenStates().observe(viewLifecycleOwner) { state ->
            when (state) {
                is Loading -> {
                    binding.shimmerLayout.startShimmer()
                }
                is Success -> {
                    binding.shimmerLayout.visibility = View.GONE
                    disneyCharactersAdapter.disneyCharacters = state.response?.data ?: emptyList()
                    binding.cartRV.adapter = disneyCharactersAdapter
                    binding.cartRV.visibility = View.VISIBLE

                }
                is ServiceError -> {
                    binding.shimmerLayout.visibility = View.GONE
                    Snackbar.make(
                        binding.root,
                        "There is some problem with this page",
                        Snackbar.LENGTH_LONG
                    ).show()
                }
                else -> {}
            }
        }

    }


}

