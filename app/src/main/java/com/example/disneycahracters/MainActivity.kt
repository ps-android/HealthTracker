package com.example.disneycahracters

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import com.example.disneycahracters.databinding.ActivityMainBinding
import com.example.disneycahracters.interfaces.AppFlowListener
import com.example.disneycahracters.viewmodel.DisneyCharacterViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity(),AppFlowListener {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.topAppBar)
    }

    override fun onSplashFragment() {
        supportActionBar?.hide()
    }

    override fun onHomeFragment() {
        supportActionBar?.show()
        binding.topAppBar.title = "Disney Characters"
    }

    override fun onDetailsFragment() {
        supportActionBar?.show()
        binding.topAppBar.title = "Details"
    }
}