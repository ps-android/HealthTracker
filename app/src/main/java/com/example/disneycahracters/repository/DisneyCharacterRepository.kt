package com.example.disneycahracters.repository

import com.example.disneycahracters.model.DisneyCharacters
import com.example.disneycahracters.database.DisneyCharacterDatabase
import com.example.disneycahracters.model.ResponseStates
import com.example.disneycahracters.model.ServiceError
import com.example.disneycahracters.model.Success
import com.example.disneycahracters.services.ApiService
import retrofit2.Response
import javax.inject.Inject

class DisneyCharacterRepository
@Inject
constructor(private val apiService: ApiService,db: DisneyCharacterDatabase) {


    private var apiResponse: DisneyCharacters? = null
    private val disneyDao = db.disneyCharDio()
    suspend fun getCharacterinfo(): ResponseStates {
        var response: Response<DisneyCharacters>? = null
        try {
            response = apiService.getCharacterinfo()

            if (response.isSuccessful) {
                apiResponse = response.body()
                if (apiResponse != null) {
                    disneyDao.deleteAllDATA()
                    disneyDao.insertDATA(apiResponse!!)
                }
            } else {
                apiResponse = disneyDao.getAllData()
            }

        } catch (exception: Exception) {
            apiResponse = disneyDao.getAllData()
            if (apiResponse != null) {
                return Success(apiResponse)
            } else ServiceError
        }

        return if (response != null && response.isSuccessful) {

            Success(apiResponse)
        } else ServiceError
    }
}