package com.example.disneycahracters.interfaces

interface AppFlowListener {

    fun onSplashFragment()

    fun onHomeFragment()

    fun onDetailsFragment()


}