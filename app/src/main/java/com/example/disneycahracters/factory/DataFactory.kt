package com.example.disneycahracters.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.disneycahracters.repository.DisneyCharacterRepository
import com.example.disneycahracters.viewmodel.DisneyCharacterViewModel
import java.lang.IllegalArgumentException

class DataFactory constructor(private val repository: DisneyCharacterRepository): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return if(modelClass.isAssignableFrom(DisneyCharacterViewModel::class.java))
        {
            DisneyCharacterViewModel(this.repository) as T
        }else{
            throw IllegalArgumentException("ViewModel Not Found")
        }
    }
}

