package com.example.disneycahracters.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.disneycahracters.model.Data
import com.example.disneycahracters.R
import com.example.disneycahracters.databinding.DisneyCharItemBinding
import javax.inject.Inject


class DisneyCharactersAdapter @Inject constructor() :
    RecyclerView.Adapter<DisneyCharactersAdapter.ViewHolder>(),Filterable {
    var disneyCharacters: List<Data> = emptyList()
    var listener: OnInteractionListener? = null

    inner class ViewHolder(private val binding: DisneyCharItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(character: Data) {
            binding.productNameTV.text = character.name
            val requestOptions = RequestOptions()
            requestOptions.placeholder(R.color.shimmer_placeholder)
            requestOptions.error(R.color.shimmer_placeholder)

            Glide.with(binding.root)
                .setDefaultRequestOptions(requestOptions).load(character.imageUrl)
                .into(binding.productIV)
            binding.productIV.contentDescription=character.name+" image"

        }
    }

    fun setOnInteractionListener(listener: OnInteractionListener) {
        this.listener = listener
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder = ViewHolder(
        DisneyCharItemBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
    )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(disneyCharacters[position])
        holder.itemView.setOnClickListener {
            listener?.onItemClicked(disneyCharacters[position])
        }
    }

    override fun getItemCount() = disneyCharacters.size

    interface OnInteractionListener {
        fun onItemClicked(data: Data)
    }

    override fun getFilter(): Filter {
        TODO("Not yet implemented")
    }
}