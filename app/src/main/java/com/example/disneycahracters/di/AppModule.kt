package com.example.disneycahracters.di

import android.app.Application
import androidx.room.Room
import com.example.disneycahracters.database.DisneyCharacterDatabase
import com.example.disneycahracters.services.ApiService
import com.example.disneycahracters.utils.Constants
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    fun provideBaseUrl() = Constants.BASE_URL

    @Provides
    @Singleton
    fun provideRetrofitInstance(BASE_URL: String): ApiService =
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(ApiService::class.java)


    @Provides
    @Singleton
    fun provideDatabase(app: Application) : DisneyCharacterDatabase =
        Room.databaseBuilder(app, DisneyCharacterDatabase::class.java, "DisneyCharacters")
            .build()

}