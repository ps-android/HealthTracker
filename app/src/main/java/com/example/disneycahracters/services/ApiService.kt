package com.example.disneycahracters.services

import com.example.disneycahracters.model.DisneyCharacters
import retrofit2.Response
import retrofit2.http.GET

interface ApiService {

    @GET("characters")
    suspend fun getCharacterinfo():Response<DisneyCharacters>
}