package com.example.disneycahracters.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.disneycahracters.model.DisneyCharacters


@Database(entities = [DisneyCharacters::class], version = 1, exportSchema = false)
@TypeConverters(Converters::class)
abstract class DisneyCharacterDatabase : RoomDatabase() {
    abstract fun disneyCharDio(): DisneyCharactersDao

    companion object {
        // Singleton prevents multiple instances of database opening at the
        // same time.
        @Volatile
        private var INSTANCE: DisneyCharacterDatabase? = null

        fun getDatabase(context: Context): DisneyCharacterDatabase {
            // if the INSTANCE is not null, then return it,
            // if it is, then create the database
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    DisneyCharacterDatabase::class.java,
                    "disney_database"
                ).build()
                INSTANCE = instance
                // return instance
                instance
            }
        }
    }
}