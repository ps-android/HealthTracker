package com.example.disneycahracters.database

import androidx.room.TypeConverter
import com.example.disneycahracters.model.Data
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type


object Converters {
    var gson = Gson()
    @TypeConverter
    fun fromString(value: String?): ArrayList<String> {
        val listType: Type = object : TypeToken<ArrayList<String?>?>() {}.getType()
        return Gson().fromJson(value, listType)
    }

    @TypeConverter
    fun fromArrayList(list: ArrayList<String?>?): String {
        val gson = Gson()
        return gson.toJson(list)
    }


    @TypeConverter
    fun dataToString(disneycharItems: ArrayList<Data>): String {
        return gson.toJson(disneycharItems)
    }

    @TypeConverter
    fun stringTodata(data: String): ArrayList<Data> {
        val listType = object : TypeToken<ArrayList<Data>>() {
        }.type
        return gson.fromJson(data, listType)
    }
}