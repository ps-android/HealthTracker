package com.example.disneycahracters.database

import androidx.room.*
import com.example.disneycahracters.model.DisneyCharacters

@Dao
interface DisneyCharactersDao {
    @Query("SELECT * FROM DisneyCharacters")
   suspend fun getAllData(): DisneyCharacters
//     fun getAllData(): Flow<DisneyCharacters>


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertDATA(disneyCharacters: DisneyCharacters)

    @Query("DELETE FROM DisneyCharacters")
    suspend fun deleteAllDATA()

}