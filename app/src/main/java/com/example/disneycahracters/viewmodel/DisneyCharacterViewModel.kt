package com.example.disneycahracters.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.disneycahracters.model.Loading
import com.example.disneycahracters.model.ResponseStates
import com.example.disneycahracters.repository.DisneyCharacterRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DisneyCharacterViewModel @Inject constructor(private val repository: DisneyCharacterRepository) :
    ViewModel() {


    private val disneychardisplayscreenStates = MutableLiveData<ResponseStates?>()
    fun getCartScreenStates() = disneychardisplayscreenStates

    init {
        getCharacterInfo()
    }


    private fun getCharacterInfo() {

        disneychardisplayscreenStates.postValue(Loading)
        viewModelScope.launch {
            val state = repository.getCharacterinfo()
            disneychardisplayscreenStates.postValue(state)
        }

    }


}
